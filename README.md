# Special characters in Openplanet

By default, Openplanet has font support for a subset of Unicode characters
and icons via the `fallbackArial` and `fallbackExtendedIcons` arguments of the
[`Resources::GetFont` method](https://openplanet.dev/docs/api/Resources/GetFont).
When loading a custom font, only the glyphs between codepoints U+0020 and
U+00FF are loaded, as defined
[in Dear ImGui](https://github.com/ocornut/imgui/blob/a61ca097a79f04ac630834919b5fa186e4bfc52c/imgui_draw.cpp#L2809-L2818).
Ideally, ImGui font support should be improved to better handle larger sets of
glyphs, but I still wanted to experiment with a method to draw a larger subset
of the full Unicode range in Openplanet. This led to this plugin, which loads a
4096×4096 texture containing the full Basic Multilingual Plane of Unicode,
using the [GNU Unifont](https://unifoundry.com/unifont/). Then, this texture is
used as a massive spritesheet to draw the characters in a window.

Note that this approach is _not at all optimized_: in some tests, it takes
approximately 0.15 ms _per character_ to draw, meaning even a very short piece
of text quickly decreases the game performance. A small table showing 128
Unicode characters simultaneously is implemented as a demo; this table already
regularly causes Openplanet to show notifications that the plugin is laggy.
However, it was still fun to play around with.
