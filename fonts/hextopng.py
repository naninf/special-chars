import json
import math
import os

from PIL import Image, ImageDraw


def main():
    hex_file = [f for f in os.listdir() if f.endswith('.hex')][0]
    with open(hex_file, 'r') as f:
        glyphs = [line.strip() for line in f.readlines()]

    im = Image.new('RGBA', (4096, 4096), (0, 0, 0, 0))
    draw = ImageDraw.Draw(im)

    for glyph in glyphs:
        codepoint, glyph_data = glyph.split(':')
        codepoint = int(codepoint, 16)
        draw_glyph(draw, codepoint, glyph_data)

    im.save(hex_file[:-4] + '.png', 'PNG')

    wide_ranges = generate_wide_ranges(glyphs)
    with open(hex_file[:-4] + '.json', 'w') as f:
        json.dump(wide_ranges, f)


def draw_glyph(draw, codepoint, glyph_data):
    coordinates = decode_glyph(glyph_data)
    x_base = codepoint % 256 * 16
    y_base = codepoint // 256 * 16

    coordinates = [(x_base + x, y_base + y) for x, y in coordinates]

    draw.point(coordinates, fill=(255, 255, 255, 255))


def decode_glyph(glyph_data):
    rows = 16
    columns = int(len(glyph_data) * 4 / rows)

    glyph_data_bin = bin(int(glyph_data, 16))[2:].zfill(len(glyph_data) * 4)
    coordinates = []
    for i, bit in enumerate(glyph_data_bin):
        if bit == '0':
            continue
        x = i % columns
        y = i // columns
        coordinates.append((x, y))

    return coordinates


def generate_wide_ranges(glyphs):
    ranges = []
    last_codepoint = None
    last_width = 0

    for glyph in glyphs:
        codepoint, glyph_data = glyph.split(':')
        codepoint = int(codepoint, 16)
        width = len(glyph_data)
        if width != last_width:
            if width == 64:
                range_start = codepoint
            else:
                range_end = last_codepoint
                ranges.append((range_start, range_end))
        last_codepoint = codepoint
        last_width = width

    return ranges


if __name__ == '__main__':
    main()
