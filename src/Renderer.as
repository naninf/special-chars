class Renderer {
    private Resources::Texture@ unifont;
    private Ranges@ wide_ranges;

    Renderer() {
        @unifont = Resources::GetTexture("fonts/unifont-14.0.01.png");

        @wide_ranges = Ranges();
        auto wide_ranges_json = Json::Parse(IO::FileSource("fonts/unifont-14.0.01.json").ReadToEnd());

        for (uint i = 0; i < wide_ranges_json.get_Length(); ++i) {
            uint start = uint(wide_ranges_json[i][0]);
            uint end = uint(wide_ranges_json[i][1]);
            wide_ranges.add_range(start, end);
        }
    }

    void RenderUtf8String(string text) {
        UnicodeString@ unicode_string = UnicodeString(text);
        RenderUnicodeString(unicode_string);
    }

    void RenderUnicodeString(UnicodeString@ unicode_string) {
        UI::PushStyleVar(UI::StyleVar::ItemSpacing, vec2(0, 0));
        for (uint i = 0; i < unicode_string.get_Length(); ++i) {
            RenderCodepoint(unicode_string[i]);
            if (i != unicode_string.get_Length() - 1) UI::SameLine();
        }
        UI::PopStyleVar();
    }

    void RenderCodepoint(uint codepoint) {
        auto draw_list = UI::GetWindowDrawList();
        vec2 pos = UI::GetWindowPos() + UI::GetCursorPos();

        uint x = codepoint % 256 * 16;
        uint y = codepoint / 256 * 16;
        uint w = (wide_ranges.in_range(codepoint)) ? 16 : 8;
        uint h = 16;

        draw_list.AddImage(unifont, pos, vec2(w, h), 0xFFFFFFFF, vec4(x, y, w, h));
        UI::Dummy(vec2(w, h));
    }
}
