class UnicodeString {
    array<uint> codepoints;

    UnicodeString(string utf8_string) {
        uint i = 0;
        while (i < uint(utf8_string.get_Length())) {
            i = decode_utf8(utf8_string, i);
        }
    }

    uint get_Length() {
        return codepoints.get_Length();
    }

    uint opIndex(uint index) {
        return codepoints[index];
    }

    private uint decode_utf8(string utf8_string, uint i) {
        if (utf8_string[i] & 0x80 == 0) {  // U+0000..U+007F
            codepoints.InsertLast(utf8_string[i]);
            return i + 1;
        } else if (utf8_string[i] & 0x20 == 0) {  // U+0080..U+07FF
            codepoints.InsertLast((utf8_string[i] & 0x1f) << 6 | (utf8_string[i + 1] & 0x3f));
            return i + 2;
        } else if (utf8_string[i] & 0x10 == 0) {  // U+0800..U+FFFF
            codepoints.InsertLast((utf8_string[i] & 0xf) << 12 | (utf8_string[i + 1] & 0x3f) << 6 | (utf8_string[i + 2] & 0x3f));
            return i + 3;
        } else {  // U+10000..U+10FFFF
            // not in BMP, so not supported by the current Unifont texture
            // if desired, these bytes can be converted as follows:
            // codepoints.insertLast((utf8_string[i] & 0x7) << 18 | (utf8_string[i + 1] & 0x3f) << 12 | (utf8_string[i + 2] & 0x3f) << 6 | (utf8_string[i + 3] & 0x3f));
            codepoints.InsertLast(0xfffd);
            return i + 4;
        }
    }
}
