Renderer@ renderer;
bool load = false;

uint rows_offset = 0;
uint render_rows = 8;
uint max_rows_offset = 4096 - render_rows;
uint column_bits = 4;
uint columns = 1 << column_bits;

void Main() {
    @renderer = Renderer();
}

void RenderInterface() {
    UI::Begin("Unicode", UI::WindowFlags::NoResize | UI::WindowFlags::AlwaysAutoResize);

    rows_offset = UI::InputInt("Start row (0-" + max_rows_offset + ")", rows_offset);

    if (rows_offset < 0) rows_offset = 0;
    if (rows_offset > max_rows_offset) rows_offset = max_rows_offset;

    if (UI::BeginTable("Unicode", columns + 1, UI::TableFlags::ScrollY)) {
        UI::TableSetupScrollFreeze(1, 1);
        UI::TableSetupColumn("Address", UI::TableColumnFlags::WidthFixed);
        for (uint i = 0; i < columns; ++i) {
            UI::TableSetupColumn(Text::Format("%x", i), UI::TableColumnFlags::WidthFixed);
        }
        UI::TableHeadersRow();

        for (uint i = 0; i < render_rows; ++i) {
            uint address_base = (i + rows_offset) * columns;
            UI::TableNextRow();
            UI::TableNextColumn();
            UI::Text(format_address(address_base));

            for (uint j = 0; j < columns; ++j) {
                UI::TableNextColumn();
                renderer.RenderCodepoint(address_base + j);
            }
        }

        UI::EndTable();
    }

    UI::End();
}

string format_address(uint address) {
    return "U+" + Text::Format("%03x", address >> column_bits) + "x";
}
