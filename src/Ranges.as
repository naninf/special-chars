class Ranges {
    array<Range@> ranges;

    void add_range(uint start, uint end) {
        ranges.InsertLast(Range(start, end));
    }

    bool in_range(uint value) {
        for (uint i = 0; i < ranges.get_Length(); ++i) {
            if (ranges[i].in_range(value)) return true;
        }
        return false;
    }
}

class Range {
    uint start;
    uint end;

    Range(uint _start, uint _end) {
        start = _start;
        end = _end;
    }

    bool in_range(uint value) {
        return value >= start && value <= end;
    }
}
